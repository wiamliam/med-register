# Med-Register


# Microservice de Gestion des Enregistrements

Ce microservice gère l'enregistrement des informations des autres microservices dans l'annuaire.

## Prérequis

- Java 11 ou une version ultérieure doit être installé.
- Configurez les fichiers de configuration dans le dossier `src/main/resources`.
- Assurez-vous que les dépendances requises sont installées en exécutant `mvn install`.

## Construction

Pour construire le microservice, exécutez la commande suivante :
mvn clean package


## Exécution

Pour exécuter le microservice, utilisez la commande suivante :

java -jar target/service-register.jar


## Documentation

Pour plus d'informations sur les points de terminaison et l'utilisation du microservice, consultez la documentation 

## Contributions

Les contributions sont les bienvenues ! Si vous souhaitez apporter des améliorations, veuillez soumettre une pull request.

## Contact

Si vous avez des questions ou des problèmes, n'hésitez pas à nous contacter à : wiamelyadri@gmail.com









